# Advance CSS / SCSS

## Three Pillars of good HTML and CSS

### Responsive Design

Build one website that works across all devices.

- Fluid Layouts - use percentages over px for all layout related lengths
- Media Queries - ensure that they adapt to the current viewport
- Mobile or Desktop First - changes the style depending on the viewport

### Maintainable and scalable code

- Clean
- Understandable
- Capable of growth

### Web performance

- Less HTTP requests
- Less code
- Compress Code
- Less images (Only nescessary images for the website)

## How CSS works behind the scenes

**What happens to css when we load up a webpage?**

- The browser loads the html
- parses html
    - loads the css and parses it
        - resolves conflicting css declarations in the **cascade step**
        - Process final css values
- loads the decoded html into the dom

**CSS also ends up in a DOM of it's own known as the CSS DOM.**

Once the css and html have been parse they will be brought together into the render tree.

The website renders using the visual formatting model.

Then you will have your final rendered website.

### How CSS is Parsed

``` css
/* css selector */
.my-class 
/* Declaration block */
{
    color: blue;
    text-align: center;
}
```

The cascade - process of combining different stylesheets and resolving conflicts between different CSS rules and declarations, when more than one rule applies to a certain elements.

- Author - the website css
- User - manual broswer changes
- Browser - broswer defaults

**Importance**

- User !important declarations
- Author !important declarations
- Author declarations
- User declarations
- Default browser declarations

If elements have the same importance then **specificity**

**Specificity**

- Inline styles 
- IDs 
- classes, pseudo classes, attributes 
- elements, pseudo elements

If everything is still equal then source order takes precidence. The **last defined style** will be the once used.

**Tips** : avoid !important, it makes maintaining css hard. Use it as a last resort.

- include your stylesheets last.

### How CSS values are processed

Note: Ultimately all **rem**, **percentages**, and other values like it are converted to **pixels**.

- Declared Value 
- Cascaded Value
- Specified Value
- Computed Value
- Used Value
- Actual Value

**rem is root reference, em is parent reference.**

### Inheritance

Every CSS property must have a value, even if it is an intial value.

If the property is inherited, the specified value will be used in the child. In the case of padding for example it's not inherited.

Properties dealing with text are generally inherited.

You can force inheritance of a property with the **inherit** keyword.

### Visual Formatting Model

Algorithm that calculates boxes and determines the layout of these boxes for each element in the render three, in order to determine the final layout of the page.

- Box type : inline, block, inline-block
- Dimensions of the box
- Stacking context

Every element on the page can be seen as a rectangle.
The rectangle consists of margin, border, padding, content (w/h).  In that order.

Padding - generates transparent area around the content inside the box.

Margin - space between boxes

Context - text, images, etc


### CSS Architecture

Think about the layout of your webpage, build your layout, and architect into different files.

#### Component Driven Design

- Modular building blocks that make up interfaces
- Held together by the layout of the page
- Reusable acros a projects and between different projects
- Idependant allowing use to use them anywhere on the page

**Research atomic design.**

BEM - Block element modifier

Block - is a standalone component that is meaningful on its own. btn

Element - part of a block that has no standalone meaning. recipe_status_block

Modifier - a different version of a block or an element. i.e. .btn-round

#### 7-1 pattern

base/
components/
layout/
pages/
themes/
abstracts/
vendors/

## Sass

``` scss
* {
  margin: 0;
  padding: 0;
}

$primary-color: #f9ed69;
$secondary-color: #f08a5d;
$tertiary-color: #b83b5e;
$dark-text: #333;
$light-text: white;

@mixin clearfix {
   &::after {
    content: "";
    clear:both;
    display:table;
  }
}

@mixin style-link-text($color) {
  text-decoration: none;
  text-transform: uppercase;
  color: $color;
}

@function divide($a, $b) {
  @return $a/$b;
}

nav {
  margin: divide(60,2) * 1px;
  background-color: $primary-color;
  @include clearfix;
}

.navigation {
  float:left;
  list-style: none;
  li {
    display:inline-block;
    margin-left: 30px;

    &:first-child {
      margin: 0;
    }

    a {
      @include style-link-text($dark-text)
    }
  }
}

.button {
  float: right;
}

%btn-placeholder {
  padding: 10px;
  display: inline-block;
  text-align: center;
  border-radius: 100px;

  width: 150px;
  @include style-link-text($light-text);
}

.btn-main {
  &:link {
    @extend %btn-placeholder;
    background-color: $secondary-color;
  }

  &:hover {
    background-color: darken($secondary-color, 15%);
  }
}

.btn-hot {
  &:link {
    @extend %btn-placeholder;
    background-color: $tertiary-color;
  }

  &:hover {
    background-color: darken($tertiary-color, 15%);
  }
}
```

## Layout Types

 - Float Layouts
 - Flexbox
 - CSS Grid

## Icons

Here: **http://linea.io/**

## Images

Here: **unsplash.com**

## Mobile First vs Desktop First

Important Note: **No matter what you do, keep both desktop and mobile in mind.**

The major difference is between using max-width (desktop first) and min-width (mobile first).

Media queries don't have any importance therefore the order of your media queries is important.

### PROS

#### Mobile First

- With mobile first development the website is 100% optimised for mobile experience
- Reduces websites and apps to the absolute essentials
- In addition results in smaller, faster, and more efficient products
- Finally it prioritizes content over aesthetic design

### CONS

#### Mobile First

- Empty feeling desktop version and simplistic
- Difficult to Develop
- Less creative freedom
- Clients are used to seeing desktop verions of sites
- Do your users use the mobile internet?

## Media Breakpoints

Important **http://gs.statcounter.com/** use to checkout the usage of devices, screen resolutions, and etc.

The bad way of selecting these is to pick just the apple devices line up and expect it to suit your needs. A good way is to go to the site above and select the most popular sizes, group them by device, then style your website accordingly. Then finally the perfect way, which requires the most time, is to scale your site until it breaks and then create a breakpoint accordingly.

## Browser Support

Many of the CSS features that you have learned are not supported, therefore you should use graceful degradation to support features in newer browsers.

## Responsive Images 

The goal is to server the right image to the right screen size and device, in order to avoid downloading unneceessary large images on smaller screens.

Use cases

- Resolution switching
- Density switching
- Art Direction (Different Image)
