var gulp = require('gulp')
    watch = require('gulp-watch')
    server = require('gulp-server-livereload')
    sass = require('gulp-sass')
    concat = require('gulp-concat')
    copy = require('gulp-copy');

gulp.task('scss', ()=>{
    gulp.src('scss/**/*.scss')
        .pipe(sass())
        .pipe(concat('style.css'))
        .pipe(gulp.dest('./css'));
});

gulp.task('fonts', ()=> {
    gulp.src('scss/fonts/**/*.*')
        .pipe(copy('css', {prefix:1}))
});

gulp.task('default', ['scss', 'fonts'], ()=>{
    gulp.src('.')
        .pipe(server({
            livereload:true,
            open:true
        }));

    gulp.watch('scss/**/*.scss', ['scss']);
    gulp.watch('scss/fonts/**/*.*', ['fonts']);
});
